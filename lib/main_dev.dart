import 'package:flutter/material.dart';
import 'package:dndroll/app/app_main.dart';
import 'package:dndroll/flavor.dart';

void main() {
  FlavorConfig(
    flavor: FlavorEnum.dev,
    onStart: () => runApp(App())
  );
}