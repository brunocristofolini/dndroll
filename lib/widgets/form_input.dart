import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:dndroll/utility/masking.dart';

class PhoneInputFormatter extends TextInputFormatter {
  @override
  TextEditingValue formatEditUpdate(
      TextEditingValue oldValue, TextEditingValue newValue) {
    var masked = MaskPhone(newValue.text);
    return TextEditingValue(
      text: masked,
      selection: TextSelection.collapsed(
        offset: masked.length,
      ),
    );
  }
}

InputDecoration buildInputDecoration(context, label) {
  return InputDecoration(
    hintText: label,
    hintStyle: TextStyle(
      color: Colors.black45,
      fontSize: 18,
      fontWeight: FontWeight.w600,
    ),
    border: UnderlineInputBorder(
      borderSide: BorderSide.none,
      borderRadius: BorderRadius.all(Radius.circular(40.0)),
    ),
    alignLabelWithHint: true,
    errorStyle: TextStyle(
      color: Color(0xFFAA0000),
      fontSize: 16,
      fontWeight: FontWeight.w600,
    ),
    filled: true,
    fillColor: Colors.white,
    focusColor: Colors.white,
    hoverColor: Colors.white,
  );
}

TextStyle buildTextStyle(context) {
  return TextStyle(color: Colors.black, fontSize: 20);
}

class TextInput extends StatefulWidget {
  TextInput({
    Key key,
    this.label = 'Texto',
    @required this.onChanged,
    @required this.onSaved,
    this.minLines = 1,
    this.maxLines = 1,
    @required this.formatter,
    @required this.validator,
    @required this.initialValue,
    this.disabled = false,
    @required this.focus,
    this.nextFocus,
    this.autofocus = false,
    this.keyboardType = TextInputType.text,
    this.align = TextAlign.start
  }) : super(key: key);

  final String label;
  final TextAlign align;
  final void Function(String) onChanged;
  final void Function(String) onSaved;
  final bool autofocus;
  final int minLines;
  final int maxLines;
  final bool disabled;
  final FormFieldValidator validator;
  final TextInputFormatter formatter;
  final String initialValue;
  final TextInputType keyboardType;
  final FocusNode focus;
  final FocusNode nextFocus;

  @override
  _TextInputState createState() => _TextInputState();
}

class _TextInputState extends State<TextInput> {
  bool dirty = false;
  int focusCount = 0;

  @override
  void initState() {
    widget.focus.addListener(() {
      if (focusCount < 3) {
        setState(() => focusCount += 1);
      }
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      enabled: !widget.disabled,
      textInputAction: TextInputAction.next,
      autovalidateMode: AutovalidateMode.onUserInteraction,
      initialValue: widget.initialValue,
      minLines: this.widget.minLines,
      maxLines: this.widget.maxLines,
      onChanged: (value) {
        if (!dirty) {
          setState(() => dirty = true);
        }
        widget.onChanged(value);
        if (widget.nextFocus != null &&
            widget.keyboardType == TextInputType.number &&
            widget.validator(value) == null) {
          debugPrint("request focus after valid number input");
          FocusScope.of(context).requestFocus(widget.nextFocus);
        }
      },
      autofocus: widget.autofocus,
      onTap: () {
        setState(() => focusCount += 1);
      },
      autocorrect: false,
      keyboardType: widget.keyboardType,
      decoration: buildInputDecoration(context, widget.label),
      style: buildTextStyle(context),
      onSaved: widget.onSaved,
      onFieldSubmitted: widget.onSaved,
      validator: widget.validator,
      inputFormatters: [widget.formatter],
      textAlign: widget.align,
    );
  }
}
