import 'package:flutter/material.dart';

class ButtonShare extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return RaisedButton(
      onPressed: () {},
      elevation: 0.0,
      color: Colors.transparent,
      textColor: Colors.white,
      child: Row(
        children: [
          Text(
            "COMPARTILHAR",
          ),
          SizedBox(
            child: Container(
              width: 10.0,
            ),
          ),
          Icon(
            Icons.share,
            color: Colors.white,
          ),
        ],
      ),
    );
  }

}