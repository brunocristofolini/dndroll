import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class ButtonFave extends StatefulWidget {
  ButtonFave({Key key, @required this.onPressed}) : super(key: key);

  final void Function(bool) onPressed;

  @override
  State<ButtonFave> createState() => _ButtonFaveState();
}

class _ButtonFaveState extends State<ButtonFave> {
  bool _buttonSelected = false;

  @override
  Widget build(BuildContext context) {
    return FlatButton(
      onPressed: () {
        setState(() {
          _buttonSelected = !_buttonSelected;
        });
        widget.onPressed(_buttonSelected);
      },
      shape: CircleBorder(),
      child: FaIcon(
        _buttonSelected
        ? FontAwesomeIcons.solidHeart
        : FontAwesomeIcons.heart,
        size: 40.0,
        color: Colors.white,
      ),
    );
  }
}