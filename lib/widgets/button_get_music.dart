import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:dndroll/bloc/user/user_export.dart';
import 'package:dndroll/bloc/user/user_bloc.dart';
import 'package:dndroll/screens/login_screen.dart';
import 'package:dndroll/widgets/alert_dialog.dart';

class ButtonGetMusic extends StatelessWidget {
  final double _defaultWidth = 130.0;

  @override
  Widget build(BuildContext context) {
    var _widthAdjust = MediaQuery.of(context).size.width / 3;
    return RaisedButton(
      color: Colors.white,
      elevation: 1.0,
      padding: EdgeInsets.all(12.0),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.all(Radius.circular(18.0)),
      ),
      child: SizedBox(
        width: _widthAdjust < _defaultWidth ? _defaultWidth : _widthAdjust,
        child: Center(
          child: Text("PEÇA SUA MÚSICA"),
        ),
      ),
      onPressed: () {
        var state = context.read<UserBloc>().state;
        if (state.status != UserStatus.authenticated) {
          showAlertDialog(
            context,
            "Ops, não temos seu telefone.",
            "Deseja informar agora?",
            (selected) {
              if (selected) {
                Navigator.push(context, LoginScreen.route());
              }
            },
          );
        }
      },
    );
  }
}
