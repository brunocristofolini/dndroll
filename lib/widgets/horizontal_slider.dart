import 'package:flutter/material.dart';
import 'package:dndroll/widgets/gesture_horizontal_detector.dart';

class HorizontalSlider extends StatefulWidget {
  const HorizontalSlider({
    Key key,
    @required this.pastChild,
    @required this.currentChild,
    @required this.direction,
  }) : super(key: key);

  final Widget pastChild;
  final Widget currentChild;
  final GestureDirection direction;

  @override
  State<HorizontalSlider> createState() => _HorizontalSliderState();
}

class _HorizontalSliderState extends State<HorizontalSlider>
    with TickerProviderStateMixin {
  AnimationController _controller;
  Animation<Offset> _animation;
  Widget _child;

  @override
  void initState() {
    super.initState();

    _controller = AnimationController(
      duration: const Duration(milliseconds: 200),
      vsync: this,
    );

    _controller.addStatusListener((status) {
      if (status == AnimationStatus.completed) {
        _resetAnimation();
        _controller.reverse();
        setState(() {
          _child = widget.currentChild;
        });
      }
    });

    _configIniState();
  }

  @override
  void didUpdateWidget(covariant HorizontalSlider oldWidget) {
    super.didUpdateWidget(oldWidget);

    _configIniState();
  }

  void _configIniState() {
    _child = widget.pastChild;
    _initAnimation(widget.direction);
    if (widget.direction != GestureDirection.none) {
      _controller.forward();
    }
  }

  @override
  Widget build(BuildContext context) {
    return SlideTransition(
      position: _animation,
      child: Container(
        child: _child,
        constraints: BoxConstraints.expand(),
        height: double.infinity,
        color: Colors.transparent,
      ),
    );
  }

  void _initAnimation(GestureDirection direction) {
    _animation =
        _buildAnimation(Offset(0.0, 0.0), _buildOffsetByDirection(direction));
  }

  void _resetAnimation() {
    _initAnimation(widget.direction == GestureDirection.right
        ? GestureDirection.left
        : GestureDirection.right);
  }

  Animation<Offset> _buildAnimation(Offset begin, Offset end) {
    return Tween<Offset>(
      begin: begin,
      end: end,
    ).animate(CurvedAnimation(
      parent: _controller,
      curve: Curves.easeInCirc,
    ));
  }

  Offset _buildOffsetByDirection(GestureDirection direction) {
    switch (direction) {
      case GestureDirection.right:
        return _buildOffsetX(-0.9);
      case GestureDirection.left:
        return _buildOffsetX(0.9);
      default:
        return _buildOffsetX(0.0);
    }
  }

  Offset _buildOffsetX(double x) {
    return Offset(x, 0.0);
  }
}
