import 'package:flutter/cupertino.dart';

class GestureHorizontalDetector extends StatefulWidget {
  const GestureHorizontalDetector(
      {Key key, @required this.child, @required this.onGesture})
      : super(key: key);

  final Widget child;
  final void Function(GestureDirection) onGesture;

  @override
  State<GestureHorizontalDetector> createState() =>
      _GestureHorizontalDetectorState();
}

enum GestureDirection { right, left, none }

class _GestureHorizontalDetectorState extends State<GestureHorizontalDetector> {
  List<double> directionX = [];

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onHorizontalDragUpdate: (DragUpdateDetails details) {
        directionX.add(details.globalPosition.dx);
      },
      onHorizontalDragEnd: (_) {
        var directionValue = directionX.last - directionX.first;
        var direction = directionValue < 0
            ? GestureDirection.right
            : GestureDirection.left;
        directionX = [];
        if (_validDrag(context, directionValue)) {
          widget.onGesture(direction);
        }
      },
      child: widget.child,
    );
  }

  bool _validDrag(BuildContext context, double directionValue) {
    var widthDevice = MediaQuery.of(context).size.width;
    var requiredWidth = widthDevice * 0.10;
    return directionValue.abs() >= requiredWidth;
  }
}
