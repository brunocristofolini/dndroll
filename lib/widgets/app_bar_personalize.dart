import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:dndroll/assets.dart';
import 'package:dndroll/bloc/user/user_bloc.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:dndroll/bloc/user/user_state.dart';
import 'package:dndroll/screens/login_screen.dart';
import 'package:dndroll/widgets/alert_dialog.dart';

class AppBarPersonalize {
  AppBarPersonalize._();

  static PreferredSize appBar(BuildContext context) {
    return PreferredSize(
      preferredSize: Size.fromHeight(130.0),
      child: AppBar(
        leading: IconButton(
          splashRadius: 24.0,
          splashColor: Color.fromRGBO(251, 111, 0, 100),
          icon: FaIcon(FontAwesomeIcons.solidUser),
          onPressed: () {
            var state = context.read<UserBloc>().state;
            if (state.status != UserStatus.authenticated) {
              showAlertDialog(
                context,
                "Ops, não temos seu telefone.",
                "Deseja informar agora?",
                    (selected) {
                  if (selected) {
                    Navigator.push(context, LoginScreen.route());
                  }
                },
              );
            }
          },
        ),
        flexibleSpace: Center(
          child: Padding(
            padding: EdgeInsets.only(
              left: 15.0,
              top: 20.0,
              bottom: 15.0,
              right: 15.0,
            ),
            child: Image.asset(Assets.imageLogo),
          ),
        ),
        elevation: 0.0,
      ),
    );
  }

}