import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class ButtonPlay extends StatefulWidget {
  ButtonPlay({Key key, @required this.onPressed}) : super(key: key);

  final void Function(bool) onPressed;

  @override
  State<ButtonPlay> createState() => _ButtonPlayState();
}

class _ButtonPlayState extends State<ButtonPlay> {
  bool _buttonSelected = false;

  @override
  Widget build(BuildContext context) {
    return FlatButton(
      onPressed: () {
        setState(() {
          _buttonSelected = !_buttonSelected;
        });
        widget.onPressed(_buttonSelected);
      },
      shape: CircleBorder(
        side: BorderSide(
          color: Colors.white.withOpacity(.6),
        ),
      ),
      child: Padding(
        padding: EdgeInsets.all(8.0),
        child: FaIcon(
          _buttonSelected
              ? FontAwesomeIcons.pauseCircle
              : FontAwesomeIcons.playCircle,
          size: 70.0,
          color: Colors.white,
        ),
      ),
    );
  }
}
