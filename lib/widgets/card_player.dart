import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:dndroll/assets.dart';
import 'package:dndroll/widgets/button_fave.dart';
import 'package:dndroll/widgets/button_play.dart';

class CardPlayer extends StatefulWidget {
  @override
  State<CardPlayer> createState() => _CardPlayerState();
}

class _CardPlayerState extends State<CardPlayer> {
  final TextStyle _textInformationStyle = TextStyle(
    fontSize: 14.0,
    fontWeight: FontWeight.w600,
    color: Colors.black87,
  );

  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 2.0,
      margin: EdgeInsets.all(10.0),
      color: Colors.transparent,
      shape: RoundedRectangleBorder(
        side: BorderSide(color: Colors.white, width: 3.0),
        borderRadius: BorderRadius.all(Radius.circular(12.0)),
      ),
      child: Container(
        width: MediaQuery.of(context).size.width,
        decoration: BoxDecoration(
          gradient: LinearGradient(
            begin: Alignment.topCenter,
            end: Alignment.bottomCenter,
            stops: [0.0, 1.0],
            colors: [
              Color(0xffffc234),
              Color(0xfffc8500),
            ],
          ),
          borderRadius: BorderRadius.all(Radius.circular(12.0)),
        ),
        child: Column(
          children: [
            Padding(
              padding: EdgeInsets.all(20.0),
              child: Center(
                child: Container(
                  width: 190.0,
                  child: Image.asset(Assets.imageLogo),
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.only(
                bottom: 25.0,
              ),
              child: Center(
                child: Text(
                  "MÚSICA AQUI!!",
                  style: _textInformationStyle,
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.only(
                bottom: 25.0,
              ),
              child: Center(
                child: Text(
                  "CANTOR AQUI!!",
                  style: _textInformationStyle,
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.only(
                bottom: 20.0,
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  FlatButton(
                    onPressed: () => null,
                    shape: CircleBorder(),
                    child: Padding(
                      padding: EdgeInsets.all(5.0),
                      child: FaIcon(
                        FontAwesomeIcons.times,
                        size: 40.0,
                        color: Colors.white,
                      ),
                    ),
                  ),
                  ButtonPlay(
                    onPressed: (selected) => null,
                  ),
                  ButtonFave(
                    onPressed: (selected) => null,
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
