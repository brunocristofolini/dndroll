import 'package:flutter/material.dart';

typedef void OnAction(bool value);

void showAlertDialog(context, title, message, OnAction onAction) {
  showDialog(
    context: context,
    builder: (BuildContext context) {
      return AlertDialog(
        title: Text(title),
        content: Text(message),
        actions: <Widget>[
          FlatButton(
            child: Text('CANCELAR'),
            color: Colors.red,
            onPressed: () {
              Navigator.of(context).pop();
              onAction(false);
            },
          ),
          FlatButton(
            child: Text('CONFIRMAR'),
            color: Colors.green,
            onPressed: () {
              Navigator.of(context).pop();
              onAction(true);
            },
          ),
        ],
      );
    },
  );
}