import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:dndroll/api/api.dart';
import 'package:dndroll/bloc/user/user_event.dart';
import 'package:dndroll/bloc/user/user_state.dart';
import 'package:dndroll/models/listener.dart';
import 'package:dndroll/models/user.dart';
import 'package:dndroll/repository/user_repository.dart';
import 'package:dndroll/setup_locator.dart';

class UserBloc extends Bloc<UserEvent, UserState> {
  final UserRepository _repository = UserRepository();
  final Api _api = locator<Api>();

  UserBloc() : super(UserState.unknown());

  @override
  Stream<UserState> mapEventToState(UserEvent event) async* {
    if (event is UserInitialize) {
      yield await _tryGetUserState();
    } else if (event is UserLogoutRequested) {
      yield await _userLogOut();
    } else {
      yield UserState.unknown();
    }
  }

  Future<UserState> _tryGetUserState() async {
    try {
      var user = await _api.verifyApp();
      await _repository.storage(user);
      if (user.listener == Listener.empty) {
        return UserState.unauthenticated(user);
      }
      return UserState.authenticated(user);
    } catch (ex) {
      print(ex.toString());
      return UserState.error(ex.toString());
    }
  }

  Future<UserState> _userLogOut() async {
    try {
      await _repository.clear();
      return UserState.unauthenticated(User.empty);
    } catch (ex) {
      print(ex.toString());
      return UserState.error(ex.toString());
    }
  }

  @override
  Future<void> close() {
    return super.close();
  }
}