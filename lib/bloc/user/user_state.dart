import 'package:equatable/equatable.dart';
import 'package:dndroll/models/user.dart';

enum UserStatus { unknown, unauthenticated, authenticated, error }

class UserState extends Equatable {
  final UserStatus status;
  final User user;
  final String error;

  const UserState._(
      {this.status = UserStatus.unknown,
      this.user = User.empty,
      this.error = ""});

  factory UserState.unknown() {
    return UserState._();
  }

  factory UserState.authenticated(User user) {
    return UserState._(
      status: UserStatus.authenticated,
      user: user,
    );
  }

  factory UserState.unauthenticated(User user) {
    return UserState._(
      status: UserStatus.unauthenticated,
      user: user,
    );
  }

  factory UserState.error(String error) {
    return UserState._(status: UserStatus.error, error: error);
  }

  @override
  String toString() {
    return status.toString();
  }

  @override
  List<Object> get props => [status, user];
}
