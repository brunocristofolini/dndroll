import 'package:equatable/equatable.dart';

abstract class UserEvent extends Equatable {
  const UserEvent();

  @override
  List<Object> get props => [];
}

class UserInitialize extends UserEvent {}

class UserLogoutRequested extends UserEvent {}