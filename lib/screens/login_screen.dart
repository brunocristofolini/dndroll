import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:dndroll/assets.dart';
import 'package:dndroll/utility/input_validation.dart';
import 'package:dndroll/utility/masking.dart';
import 'package:dndroll/widgets/container_gradient.dart';
import 'package:dndroll/widgets/form_input.dart';

class LoginScreen extends StatefulWidget {

  static Route route() {
    return MaterialPageRoute(builder: (_) => LoginScreen());
  }

  @override
  State<LoginScreen> createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  final FocusNode cellPhoneFocus = FocusNode();
  final FocusNode focus = FocusNode();
  String phone = "";

  @override
  Widget build(BuildContext context) {
    return ContainerGradient(
      child: Scaffold(
        body: Container(
          padding: EdgeInsets.only(
            left: 20.0,
            right: 20.0,
          ),
          child: Column(
            children: [
              Padding(
                padding: EdgeInsets.only(
                  top: 70.0,
                ),
                child: Center(
                  child: Container(
                    width: 190.0,
                    child: Image.asset(Assets.imageLogo),
                  ),
                ),
              ),
              Padding(
                padding: EdgeInsets.only(
                  top: 40.0,
                ),
                child: Text(
                  "Entre com seu número de telefone",
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    color: Color(0xffffeb3b),
                    fontSize: 25.0,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
              Padding(
                padding: EdgeInsets.only(
                  top: 30.0,
                ),
                child: TextInput(
                  focus: cellPhoneFocus,
                  keyboardType: TextInputType.number,
                  label: "Digite seu DDD + Número",
                  initialValue: MaskPhone(phone),
                  validator: InputValidation.phone,
                  formatter: PhoneInputFormatter(),
                  align: TextAlign.center,
                  onChanged: (value) {
                    setState(() => {});
                  },
                  onSaved: (value) {
                    FocusScope.of(context).requestFocus(focus);
                  },
                  nextFocus: focus,
                ),
              ),
              Padding(
                padding: EdgeInsets.only(
                  top: 30.0,
                ),
                child: RaisedButton(
                  color: Color(0xfffbc02d),
                  elevation: 2.0,
                  padding: EdgeInsets.all(12.0),
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.all(Radius.circular(40.0)),
                  ),
                  child: Center(
                    child: Text(
                      "CONTINUAR",
                      style: TextStyle(
                        fontSize: 18.0,
                        fontWeight: FontWeight.bold,
                        color: Colors.white,
                      ),
                    ),
                  ),
                  onPressed: () => null,
                ),
              ),
              Expanded(child: Container()),
              Padding(
                padding: EdgeInsets.only(bottom: 70.0),
                child: TextButton(
                  onPressed: () => Navigator.pop(context),
                  child: Text(
                    "Cancelar",
                    style: TextStyle(
                      color: Colors.white,
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
