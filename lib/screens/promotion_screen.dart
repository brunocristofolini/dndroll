import 'package:flutter/material.dart';

class PromotionScreen extends StatefulWidget {
  @override
  State<PromotionScreen> createState() => _PromotionScreenState();
}

class _PromotionScreenState extends State<PromotionScreen> {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Center(
          child: Padding(
            padding: EdgeInsets.only(top: 45.0, bottom: 20.0),
            child: Text(
              "Promoções",
              style: Theme.of(context).textTheme.headline4,
            ),
          ),
        ),
        Expanded(
          child: Container(
            child: GridView.count(
              crossAxisCount: 2,
              shrinkWrap: true,
              physics: ScrollPhysics(),
              children: List.generate(
                6,
                (index) => Padding(
                  padding: EdgeInsets.all(10.0),
                  child: Center(
                    child: Container(
                      color: Colors.red,
                    ),
                  ),
                ),
              ),
            ),
          ),
        ),
      ],
    );
  }
}
