import 'package:flutter/foundation.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:webview_flutter/webview_flutter.dart';

class WebViewScreen extends StatefulWidget {
  final String url;

  const WebViewScreen({Key key, this.url}) : super(key: key);

  @override
  State<WebViewScreen> createState() => _WebViewScreenState(this.url);
}

class _WebViewScreenState extends State<WebViewScreen> {
  final String _url;
  int _index = 0;

  _WebViewScreenState(this._url);

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return IndexedStack(
      index: _index,
      children: [
        Container(
          color: Colors.transparent,
          child: Center(child: CircularProgressIndicator()),
        ),
        WebView(
          key: widget.key,
          javascriptMode: JavascriptMode.unrestricted,
          gestureRecognizers: Set()
            ..add(Factory<VerticalDragGestureRecognizer>(
                    () => VerticalDragGestureRecognizer())),
          initialUrl: _url,
          onPageStarted: (_) => startLoading(),
          onPageFinished: (_) => doneLoading(),
        ),
      ],
    );
  }

  @override
  void dispose() {
    super.dispose();
  }

  void startLoading() {
    setState(() {
      _index = 0;
    });
  }

  void doneLoading() {
    setState(() {
      _index = 1;
    });
  }
}
