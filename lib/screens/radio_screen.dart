import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:dndroll/widgets/button_get_music.dart';
import 'package:dndroll/widgets/button_share.dart';
import 'package:dndroll/widgets/card_player.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class RadioScreen extends StatefulWidget {
  @override
  State<RadioScreen> createState() => _RadioScreenState();
}

class _RadioScreenState extends State<RadioScreen> {
  @override
  Widget build(BuildContext context) {
    return ListView(
      padding: EdgeInsets.all(8.0),
      children: <Widget>[
        Padding(
          padding: EdgeInsets.only(top: 25.0),
          child: Center(
            child: ButtonGetMusic(),
          ),
        ),
        CardPlayer(),
        Padding(
          padding: EdgeInsets.only(
            left: 10.0,
            right: 10.0,
          ),
          child: Row(
            children: [
              ButtonShare(),
              Expanded(
                child: Container(),
              ),
              Row(
                children: [
                  IconButton(
                    icon: FaIcon(FontAwesomeIcons.facebook),
                    onPressed: () => null,
                    splashRadius: 25.0,
                  ),
                  IconButton(
                    icon: FaIcon(FontAwesomeIcons.instagram),
                    onPressed: () => null,
                    splashRadius: 25.0,
                  ),
                ],
              ),
            ],
          ),
        ),
      ],
    );
  }
}
