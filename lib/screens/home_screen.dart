import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:dndroll/bloc/user/user_export.dart';
import 'package:dndroll/models/company_configs.dart';
import 'package:dndroll/screens/promotion_screen.dart';
import 'package:dndroll/screens/radio_screen.dart';
import 'package:dndroll/screens/web_view_screen.dart';
import 'package:dndroll/widgets/app_bar_personalize.dart';
import 'package:dndroll/widgets/container_gradient.dart';
import 'package:dndroll/widgets/gesture_horizontal_detector.dart';
import 'package:dndroll/widgets/horizontal_slider.dart';

class HomeScreen extends StatefulWidget {
  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  int _selectedIndex = 0;
  int _lastSelectedIndex = 0;
  GestureDirection _direction = GestureDirection.none;
  CompanyConfigs _company = CompanyConfigs.empty;

  @override
  void initState() {
    super.initState();
    _initCompanyConfigs();
  }

  List<Widget> _widgetOptions = <Widget>[
    RadioScreen(),
    PromotionScreen(),
  ];

  List<BottomNavigationBarItem> _items = <BottomNavigationBarItem>[
    _buildItem(FontAwesomeIcons.play),
    _buildItem(FontAwesomeIcons.gift),
  ];

  @override
  Widget build(BuildContext context) {
    return ContainerGradient(
      child: Scaffold(
        appBar: AppBarPersonalize.appBar(context),
        body: GestureHorizontalDetector(
          child: HorizontalSlider(
            pastChild: _widgetOptions.elementAt(_lastSelectedIndex),
            currentChild: _widgetOptions.elementAt(_selectedIndex),
            direction: _direction,
          ),
          onGesture: (direction) {
            _onGestureDetect(direction);
          },
        ),
        bottomNavigationBar: BottomNavigationBar(
          items: _items,
          selectedItemColor: Color(0xffffee58),
          currentIndex: _selectedIndex,
          onTap: _onItemTapped,
        ),
      ),
    );
  }

  static BottomNavigationBarItem _buildItem(IconData icon) {
    return BottomNavigationBarItem(
      icon: FaIcon(icon),
      label: '',
      backgroundColor: Colors.transparent,
    );
  }

  void _initCompanyConfigs() {
    _company = context.read<UserBloc>().state.user?.companyConfigs
    ?? CompanyConfigs.empty;

    if (_company != CompanyConfigs.empty) {
      _buildWidgetOptions();
      _buildItemsBar();
    }
  }

  void _buildWidgetOptions() {
    _widgetOptions.addAll(_company.personalContents.map((personal) {
      return WebViewScreen(key: UniqueKey(), url: personal.url);
    }));
  }

  void _buildItemsBar() {
    _items.addAll(_company.personalContents.map((personal) {
      return _buildItem(personal.icon);
    }));
  }

  int _getIndexByDirection(GestureDirection direction) {
    const int startIndex = 0;
    var maxIndex = _widgetOptions.length - 1;
    var rightIndex = _selectedIndex + 1;
    var leftIndex = _selectedIndex - 1;
    switch (direction) {
      case GestureDirection.right:
        return rightIndex > maxIndex ? startIndex : rightIndex;
      default:
        return leftIndex < startIndex ? maxIndex : leftIndex;
    }
  }

  void _onGestureDetect(GestureDirection direction) {
    _setIndex(_getIndexByDirection(direction), direction: direction);
  }

  void _onItemTapped(int index) {
    _setIndex(index);
  }

  void _setIndex(int index, {GestureDirection direction}) {
    if (direction == null) {
      direction = _convertIndexInDirection(index);
    }

    setState(() {
      _lastSelectedIndex = _selectedIndex;
      _selectedIndex = index;
      _direction = direction;
    });
  }

  GestureDirection _convertIndexInDirection(int index) {
    var result = _selectedIndex - index;
    if (result == 0) return GestureDirection.none;
    return result > 0 ? GestureDirection.left : GestureDirection.right;
  }
}
