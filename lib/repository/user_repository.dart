import 'dart:convert';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:dndroll/models/user.dart';
import 'package:dndroll/setup_locator.dart';

class UserRepository {
  static UserRepository _instance;

  factory UserRepository(){
    _instance ??= UserRepository._internal();
    return _instance;
  }

  UserRepository._internal();

  final FlutterSecureStorage _storage = locator<FlutterSecureStorage>();
  User _user = User.empty;

  Future<User> getUser() async {
    if (_user != User.empty){
      return _user;
    }

    var jsonString = await _storage.read(key: 'user');
    if (jsonString == null) {
      _user = User.empty;
      return _user;
    }

    Map<String, dynamic> map = jsonDecode(jsonString);
    _user = User.fromJson(map);
    return _user;
  }

  Future storage(User newUser) async {
    print("-=------------------");
    print(json.encode(newUser));
    print("-=------------------");
    await _storage.write(key: 'user', value: json.encode(newUser));
    _user = newUser;
  }

  Future clear() async {
    await _storage.delete(key: 'user');
    _user = User.empty;
  }
}