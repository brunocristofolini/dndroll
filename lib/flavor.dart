import 'package:flutter/cupertino.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/intl.dart';
import 'package:dndroll/bloc/app_bloc_observer.dart';
import 'package:dndroll/setup_locator.dart';
import 'package:simple_moment/simple_moment.dart';

enum FlavorEnum { prod, hom, dev }

class FlavorConfig {
  final FlavorEnum flavor;

  static FlavorConfig _instance;

  factory FlavorConfig({
    @required FlavorEnum flavor,
    @required Function() onStart,
  }) {
    _start(onStart);
    _instance ??= FlavorConfig._internal(flavor);
    return _instance;
  }

  FlavorConfig._internal(this.flavor);

  static void _start(Function() onStart){
    Moment.setLocaleGlobally(new LocalePtBr());
    Intl.defaultLocale = 'pt_BR';
    WidgetsFlutterBinding.ensureInitialized();
    setupLocator();
    Bloc.observer = AppBlocObserver();
    SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]).then(
        (_) => onStart(),
    );
  }

  static bool get isDevelop => _instance.flavor == FlavorEnum.dev;
  static bool get isProduction => _instance.flavor == FlavorEnum.prod;
  static bool get isHomologation => _instance.flavor == FlavorEnum.hom;
}
