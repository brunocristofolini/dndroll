String MaskPhone(value) {
  var text = value.replaceAll(RegExp(r'[^\d]'), "");
  if (text.length > 11) {
    text = text.substring(0, 11);
  }
  if (text.length == 0) {
    return "";
  }
  if (text.length <= 2) {
    return "(" + text;
  }
  if (text.length >= 3 && text.length <= 7) {
    return "(" + text.substring(0, 2) + ") " + text.substring(2);
  }
  if (text.length >= 8 && text.length <= 11) {
    return ("(" +
        text.substring(0, 2) +
        ") " +
        text.substring(2, 7) +
        "-" +
        text.substring(7));
  }
  return text;
}