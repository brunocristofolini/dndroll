class InputValidation {
  InputValidation._();

  static bool _isValidPhone(String val) {
    if (RegExp(r'^\(\d{2}\) \d{5}-\d{4}$').hasMatch(val)) {
      return true;
    }
    return false;
  }

  static String phone(val) {
    if (!(val is String)) {
      return "Campo obrigatório.";
    }
    if (val == null || val == "") {
      return "Campo obrigatório.";
    }
    if (_isValidPhone(val)) {
      return null;
    }
    return "Formato esperado: (99) 12345-1234.";
  }
}