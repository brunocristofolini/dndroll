import 'package:http/http.dart';
import 'package:dndroll/api/api_base.dart';
import 'package:dndroll/models/user.dart';

class Api {
  ApiBase _apiBase = ApiBase(Client());

  Api(Client client) {
    _apiBase = ApiBase(client);
  }

  Future<User> verifyApp() async {
    final response = await _apiBase.post(
      '/hotsites-frame/verify-app',
      '{"company":"1d02e59b-2b4a-41ad-8fe9-e0206f3c718f", "cellphone":""}',
    );
    var user = User.fromJson(response);
    return Future.value(user);
  }
}