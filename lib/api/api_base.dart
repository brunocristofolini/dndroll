import 'dart:convert';
import 'dart:io';
import 'package:http/http.dart';
import 'package:dndroll/api/api_exceptions.dart';
import 'package:dndroll/services/url_service.dart';

class ApiBase {
  final String _baseUrl = UrlService.get();
  final Client _client;

  ApiBase(this._client);

  Future<dynamic> get(String url) async {
    print('Api Get, url $_baseUrl $url');
    var responseJson;
    try {
      final response = await _client.get(
        _baseUrl + url,
        headers: _getHeaders(),
      );
      responseJson = _returnResponse(response);
    } on SocketException catch (e) {
      print('No net');
      print(e);
      print(e.osError);
      throw FetchDataException('No Internet connection');
    }
    print('api get received!');
    return responseJson;
  }

  Future<dynamic> post(String url, dynamic body) async {
    print('Api Post, Url: ${_baseUrl + url}');
    var responseJson;
    try {
      final response = await _client.post(
        _baseUrl + url,
        body: body,
        headers: _getHeaders(),
      );
      responseJson = _returnResponse(response);
    } on SocketException catch (e) {
      print('No net');
      print(e);
      print(e.osError);
      throw FetchDataException('No Internet connection');
    }
    print('api post received!');
    return responseJson;
  }

  Map<String, String> _getHeaders() {
    var headers = new Map<String, String>();
    headers[HttpHeaders.contentTypeHeader] = 'application/json';
    return headers;
  }

  dynamic _returnResponse(Response response) {
    switch (response.statusCode) {
      case 200:
        var responseJson = json.decode(response.body.toString());
        print(responseJson);
        return responseJson;
        break;
      case 400:
        throw BadRequestException(response.body.toString());
        break;
      case 401:
        throw UnauthorisedException(response.body.toString());
        break;
      case 403:
        throw ForbiddenException(response.body.toString());
        break;
      case 500:
      default:
        throw FetchDataException(
            'Error occurred while Communication with Server with StatusCode : ${response.statusCode}');
    }
  }
}
