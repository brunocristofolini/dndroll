class ApiException implements Exception {
  final _message;
  final _prefix;

  ApiException([this._message, this._prefix]);

  String toString() {
    return '$_prefix$_message';
  }

  String userMessage() {
    return 'Ocorreu um erro interno ao acessar o servidor.';
  }
}

class FetchDataException extends ApiException {
  FetchDataException([String message = ""])
      : super(message, 'Error During Communication: ');

  @override
  String userMessage() {
    return 'Ocorreu um erro ao acessar o servidor. Favor conferir sua conexão com a Internet.';
  }
}

class BadRequestException extends ApiException {
  BadRequestException([message]) : super(message, 'Invalid Request: ');
}

class UnauthorisedException extends ApiException {
  UnauthorisedException([message]) : super(message, 'Unauthorised: ');
}

class ForbiddenException extends ApiException {
  ForbiddenException([message]) : super(message, 'Forbidden: ');
}

class InvalidInputException extends ApiException {
  InvalidInputException([String message = ""])
      : super(message, 'Invalid Input: ');
}

class UnacceptableDataException extends ApiException {
  UnacceptableDataException([String message = ""])
      : super(message, 'Invalid data (422): ');
}

class DuplicatedDataException extends ApiException {
  DuplicatedDataException([String message = ""])
      : super(message, 'Duplicate data (409): ');

  @override
  String userMessage() {
    return 'Ocorreu um erro de dados duplicados. Favor verificar os dados enviados.';
  }
}
