import 'package:dndroll/flavor.dart';

class UrlService {
  UrlService._();

  static String get() {
    return FlavorConfig.isProduction
        ? "https://app.audients.com.br:4002"
        : "https://app.audients.com.br:4003";
  }
}
