import 'package:flutter/material.dart';

class NavigationService {
  final GlobalKey<NavigatorState> navigatorKey =
  new GlobalKey<NavigatorState>();

  Future<dynamic> navigateTo(String routeName) {
    return navigatorKey.currentState.pushNamed(routeName);
  }

  Future<dynamic> navigateToWidget(Widget w) {
    return navigatorKey.currentState.push(
      MaterialPageRoute(builder: (context) => w),
    );
  }

  Future<dynamic> resetWithWidget(Widget w) {
    return navigatorKey.currentState.pushAndRemoveUntil(
      MaterialPageRoute(builder: (context) => w),
          (Route<dynamic> route) => false,
    );
  }
}