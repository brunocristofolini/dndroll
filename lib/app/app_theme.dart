import 'package:flutter/material.dart';

class AppTheme {
  AppTheme._();

  static final String appName = "D&D Roll";

  static final ThemeData lightTheme = ThemeData(
    scaffoldBackgroundColor: Colors.transparent,
    primaryColor: Color(0xfffb6f00),
    primarySwatch: Colors.orange,
    accentColor: Colors.redAccent,
    appBarTheme: AppBarTheme(
      color: Colors.transparent,
      iconTheme: IconThemeData(
        color: Colors.black,
      ),
    ),
    colorScheme: ColorScheme.light(
      primary: Color(0xfffb6f00),
      onPrimary: Colors.white,
      primaryVariant: Color(0xffffc234),
      secondary: Colors.white,
    ),
    cardTheme: CardTheme(
      color: Color(0xfffaf3f0),
    ),
    iconTheme: IconThemeData(
      color: Colors.white,
    ),
    textTheme: TextTheme(
      headline4: TextStyle(
        color: Colors.white,
        fontSize: 20.0,
        fontWeight: FontWeight.bold
      ),
      subtitle2: TextStyle(
        color: Colors.white70,
        fontSize: 18.0,
      ),
    ),
    bottomNavigationBarTheme: BottomNavigationBarThemeData(
      backgroundColor: Colors.transparent,
      elevation: 0.0,
    ),
    fontFamily: 'Roboto',
    visualDensity: VisualDensity.adaptivePlatformDensity,
  );

  //Implemente o seu tema escuro :)
  static final ThemeData darkTheme = ThemeData(

  );
}