import 'dart:io';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:dndroll/app/app_theme.dart';
import 'package:dndroll/bloc/user/user_export.dart';
import 'package:dndroll/screens/home_screen.dart';
import 'package:dndroll/screens/splash_screen.dart';
import 'package:dndroll/services/navigation_service.dart';
import 'package:dndroll/setup_locator.dart';
import 'package:webview_flutter/webview_flutter.dart';

final RouteObserver<PageRoute> routeObserver = RouteObserver<PageRoute>();

class App extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider<UserBloc>(create: (context) => UserBloc()),
      ],
      child: AppView(),
    );
  }
}

class AppView extends StatefulWidget {
  @override
  State<AppView> createState() => _AppViewState();
}

class _AppViewState extends State<AppView> {
  final navigator = locator<NavigationService>();

  @override
  void initState() {
    super.initState();
    context.read<UserBloc>().add(UserInitialize());
    if (Platform.isAndroid) WebView.platform = SurfaceAndroidWebView();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: AppTheme.appName,
      localizationsDelegates: [
        GlobalCupertinoLocalizations.delegate,
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate
      ],
      supportedLocales: [const Locale('pt', 'BR')],
      navigatorKey: navigator.navigatorKey,
      theme: AppTheme.lightTheme,
      debugShowCheckedModeBanner: false,
      builder: (context, child) {
        return BlocListener<UserBloc, UserState>(
          listener: (context, state) {
            switch (state.status) {
              case UserStatus.unknown:
                navigator.resetWithWidget(SplashScreen());
                break;
              default:
                navigator.resetWithWidget(HomeScreen());
                break;
            }
          },
          child: child,
        );
      },
      navigatorObservers: [routeObserver],
      onGenerateRoute: (_) => SplashScreen.route(),
    );
  }
}
