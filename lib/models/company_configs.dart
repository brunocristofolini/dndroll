import 'package:equatable/equatable.dart';
import 'package:dndroll/models/personal_content.dart';
import 'package:dndroll/models/social_media.dart';
import 'package:json_annotation/json_annotation.dart';

part 'company_configs.g.dart';

@JsonSerializable(nullable: true)
class CompanyConfigs extends Equatable {
  const CompanyConfigs(
    this.uuid,
    this.defaultCompany, {
    this.shareMessage = "",
    this.shareLink = "",
    this.logoUrl = "",
    this.stream = "",
    this.personalContents = const <PersonalContent>[],
    this.socialMedias = const <SocialMedia>[],
  });

  final String uuid;
  final String stream;

  @JsonKey(name: "default_company",)
  final String defaultCompany;

  @JsonKey(name: "share_message",)
  final String shareMessage;

  @JsonKey(name: "share_link",)
  final String shareLink;

  @JsonKey(name: "logo_url",)
  final String logoUrl;

  @JsonKey(name: "personal_content", defaultValue: const <PersonalContent>[])
  final List<PersonalContent> personalContents;

  @JsonKey(name: "social_media", defaultValue: const <PersonalContent>[])
  final List<SocialMedia> socialMedias;

  factory CompanyConfigs.fromJson(Map<String, dynamic> json) =>
      _$CompanyConfigsFromJson(json);

  Map<String, dynamic> toJson() => _$CompanyConfigsToJson(this);

  static const empty = CompanyConfigs("", "");

  @override
  List<Object> get props => [uuid];
}
