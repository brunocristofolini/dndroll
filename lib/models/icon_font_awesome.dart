import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:dndroll/models/icons_map.dart';

class IconFontAwesome {
  IconFontAwesome._();

  static IconData convert(String icon) {
    if (IconsMap.containsKey(icon)) {
      return IconsMap[icon];
    }
    return FontAwesomeIcons.paste;
  }

  static String convertIconData(IconData icon) {
    return _getIcon(icon);
  }

  static String _getIcon(IconData icon) {
    var defaultIcon = "fas fa-paste";
    IconsMap.forEach((key, value) {
      if (value.codePoint == icon.codePoint) {
        defaultIcon = key;
      }
    });
    return defaultIcon;
  }
}