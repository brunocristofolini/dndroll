import 'package:equatable/equatable.dart';
import 'package:dndroll/models/company_configs.dart';
import 'package:dndroll/models/listener.dart';
import 'package:json_annotation/json_annotation.dart';

part 'user.g.dart';

@JsonSerializable(nullable: true)
class User extends Equatable {
  const User(
    this.token, {
    this.listener = Listener.empty,
    this.companyConfigs = CompanyConfigs.empty,
  });

  final String token;

  @JsonKey(name: "listener", fromJson: _listenerFromJson)
  final Listener listener;

  @JsonKey(name: "company_configs", fromJson: _companyFromJson)
  final CompanyConfigs companyConfigs;

  static const User empty = User("");

  static CompanyConfigs _companyFromJson(Map<String, dynamic> company) =>
      company == null ? CompanyConfigs.empty : CompanyConfigs.fromJson(company);

  static Listener _listenerFromJson(Map<String, dynamic> listener) =>
      listener == null ? Listener.empty : CompanyConfigs.fromJson(listener);

  factory User.fromJson(Map<String, dynamic> json) => _$UserFromJson(json);

  Map<String, dynamic> toJson() => _$UserToJson(this);

  @override
  List<Object> get props => [token];
}
