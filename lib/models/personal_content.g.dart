// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'personal_content.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

PersonalContent _$PersonalContentFromJson(Map<String, dynamic> json) {
  return PersonalContent(
    json['uuid'] as String,
    url: json['url'] as String,
    icon: PersonalContent._iconFromJson(json['icon'] as String),
    appUuid: json['app_configuration_uuid'] as String,
  );
}

Map<String, dynamic> _$PersonalContentToJson(PersonalContent instance) =>
    <String, dynamic>{
      'uuid': instance.uuid,
      'url': instance.url,
      'icon': PersonalContent._iconToJson(instance.icon),
      'app_configuration_uuid': instance.appUuid,
    };
