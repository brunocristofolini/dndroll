// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'listener.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Listener _$ListenerFromJson(Map<String, dynamic> json) {
  return Listener(
    json['uuid'] as String,
    fullName: json['fullname'] as String,
    cpf: json['cpf'] as String,
    birthdate: Listener._birthdateFromJson(json['birthdate'] as DateTime),
    gender: json['gender'] as String,
    cellphone: json['cellphone'] as String,
    address: Listener._addressFromJson(json['address'] as Map<String, dynamic>),
  );
}

Map<String, dynamic> _$ListenerToJson(Listener instance) => <String, dynamic>{
      'uuid': instance.uuid,
      'fullname': instance.fullName,
      'cpf': instance.cpf,
      'birthdate': instance.birthdate?.toIso8601String(),
      'gender': instance.gender,
      'cellphone': instance.cellphone,
      'address': instance.address,
    };
