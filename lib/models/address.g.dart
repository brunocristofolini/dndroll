// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'address.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Address _$AddressFromJson(Map<String, dynamic> json) {
  return Address(
    json['uuid'] as String,
    cep: json['cep'] as String,
    country: json['country'] as String,
    state: json['state'] as String,
    city: json['city'] as String,
    neighborhood: json['neighborhood'] as String,
    street: json['street'] as String,
    number: json['number'] as String,
  );
}

Map<String, dynamic> _$AddressToJson(Address instance) => <String, dynamic>{
      'uuid': instance.uuid,
      'cep': instance.cep,
      'country': instance.country,
      'state': instance.state,
      'city': instance.city,
      'neighborhood': instance.neighborhood,
      'street': instance.street,
      'number': instance.number,
    };
