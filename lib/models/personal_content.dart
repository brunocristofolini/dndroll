import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:dndroll/models/icon_font_awesome.dart';
part 'personal_content.g.dart';

@JsonSerializable(nullable: true)
class PersonalContent extends Equatable {
  const PersonalContent(
    this.uuid, {
    this.url = "",
    this.icon,
    this.appUuid = "",
  });

  final String uuid;
  final String url;

  @JsonKey(fromJson: _iconFromJson, toJson: _iconToJson)
  final IconData icon;

  @JsonKey(name: "app_configuration_uuid")
  final String appUuid;
  static IconData _iconFromJson(String icon) {
    return icon == null || icon.isEmpty
        ? FontAwesomeIcons.paste
        : IconFontAwesome.convert(icon);
  }

  static String _iconToJson(IconData icon) {
    return icon == null
        ? "fas fa-paste"
        : IconFontAwesome.convertIconData(icon);
  }

  factory PersonalContent.fromJson(Map<String, dynamic> json) =>
      _$PersonalContentFromJson(json);

  Map<String, dynamic> toJson() => _$PersonalContentToJson(this);

  static const empty = PersonalContent("");

  @override
  List<Object> get props => [uuid];
}
