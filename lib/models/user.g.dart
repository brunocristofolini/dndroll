// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'user.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

User _$UserFromJson(Map<String, dynamic> json) {
  return User(
    json['token'] as String,
    listener: User._listenerFromJson(json['listener'] as Map<String, dynamic>),
    companyConfigs:
        User._companyFromJson(json['company_configs'] as Map<String, dynamic>),
  );
}

Map<String, dynamic> _$UserToJson(User instance) => <String, dynamic>{
      'token': instance.token,
      'listener': instance.listener,
      'company_configs': instance.companyConfigs,
    };
