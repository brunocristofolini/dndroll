import 'package:equatable/equatable.dart';
import 'package:dndroll/models/address.dart';
import 'package:json_annotation/json_annotation.dart';

part 'listener.g.dart';

@JsonSerializable(nullable: true)
class Listener extends Equatable {
  const Listener(this.uuid,
      {this.fullName = "",
      this.cpf = "",
      this.birthdate,
      this.gender = "male",
      this.cellphone = "",
      this.address = Address.empty});

  final String uuid;

  @JsonKey(name: "fullname")
  final String fullName;
  final String cpf;

  @JsonKey(fromJson: _birthdateFromJson)
  final DateTime birthdate;
  final String gender;
  final String cellphone;

  @JsonKey(fromJson: _addressFromJson)
  final Address address;

  static Address _addressFromJson(Map<String, dynamic> address) =>
      address == null ? Address.empty : Address.fromJson(address);

  static DateTime _birthdateFromJson(DateTime date) =>
      date == null ? DateTime(1990, 1, 1) : date;

  factory Listener.fromJson(Map<String, dynamic> json) =>
      _$ListenerFromJson(json);

  Map<String, dynamic> toJson() => _$ListenerToJson(this);

  static const empty = Listener("");

  @override
  List<Object> get props => [uuid];
}
