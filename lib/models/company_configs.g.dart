// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'company_configs.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CompanyConfigs _$CompanyConfigsFromJson(Map<String, dynamic> json) {
  return CompanyConfigs(
    json['uuid'] as String,
    json['default_company'] as String,
    shareMessage: json['share_message'] as String,
    shareLink: json['share_link'] as String,
    logoUrl: json['logo_url'] as String,
    stream: json['stream'] as String,
    personalContents: (json['personal_content'] as List)
            ?.map((e) => e == null
                ? null
                : PersonalContent.fromJson(e as Map<String, dynamic>))
            ?.toList() ??
        [],
    socialMedias: (json['social_media'] as List)
            ?.map((e) => e == null
                ? null
                : SocialMedia.fromJson(e as Map<String, dynamic>))
            ?.toList() ??
        [],
  );
}

Map<String, dynamic> _$CompanyConfigsToJson(CompanyConfigs instance) =>
    <String, dynamic>{
      'uuid': instance.uuid,
      'stream': instance.stream,
      'default_company': instance.defaultCompany,
      'share_message': instance.shareMessage,
      'share_link': instance.shareLink,
      'logo_url': instance.logoUrl,
      'personal_content': instance.personalContents,
      'social_media': instance.socialMedias,
    };
