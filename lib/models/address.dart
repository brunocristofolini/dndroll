import 'package:equatable/equatable.dart';
import 'package:json_annotation/json_annotation.dart';

part 'address.g.dart';

@JsonSerializable(nullable: true)
class Address extends Equatable {
  const Address(
    this.uuid, {
    this.cep = "",
    this.country = "",
    this.state = "",
    this.city = "",
    this.neighborhood = "",
    this.street = "",
    this.number = "",
  });

  final String uuid;
  final String cep;
  final String country;
  final String state;
  final String city;
  final String neighborhood;
  final String street;
  final String number;

  factory Address.fromJson(Map<String, dynamic> json) => _$AddressFromJson(json);

  Map<String, dynamic> toJson() => _$AddressToJson(this);

  static const empty = Address("");

  @override
  List<Object> get props => [uuid];
}
