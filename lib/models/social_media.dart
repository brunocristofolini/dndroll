import 'package:equatable/equatable.dart';
import 'package:json_annotation/json_annotation.dart';

part 'social_media.g.dart';

@JsonSerializable(nullable: true)
class SocialMedia extends Equatable {
  const SocialMedia(
    this.uuid, {
    this.type = "",
    this.token = "",
    this.appUuid = "",
    this.url = "",
    this.companyUuid = "",
  });

  final String uuid;
  final String type;
  final String token;
  final String url;

  @JsonKey(name: "app_configuration_uuid")
  final String appUuid;

  @JsonKey(name: "company_uuid")
  final String companyUuid;

  factory SocialMedia.fromJson(Map<String, dynamic> json) =>
      _$SocialMediaFromJson(json);

  Map<String, dynamic> toJson() => _$SocialMediaToJson(this);

  static const empty = SocialMedia("");

  @override
  List<Object> get props => [uuid];
}
