// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'social_media.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

SocialMedia _$SocialMediaFromJson(Map<String, dynamic> json) {
  return SocialMedia(
    json['uuid'] as String,
    type: json['type'] as String,
    token: json['token'] as String,
    appUuid: json['app_configuration_uuid'] as String,
    url: json['url'] as String,
    companyUuid: json['company_uuid'] as String,
  );
}

Map<String, dynamic> _$SocialMediaToJson(SocialMedia instance) =>
    <String, dynamic>{
      'uuid': instance.uuid,
      'type': instance.type,
      'token': instance.token,
      'url': instance.url,
      'app_configuration_uuid': instance.appUuid,
      'company_uuid': instance.companyUuid,
    };
