import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:get_it/get_it.dart';
import 'package:http/http.dart';
import 'package:dndroll/services/navigation_service.dart';
import 'api/api.dart';

GetIt locator = GetIt.instance;

void setupLocator() {
  var client = Client();
  locator.registerLazySingleton(() => Api(client));
  locator.registerLazySingleton(() => NavigationService());
  locator.registerLazySingleton(() => FlutterSecureStorage());
}